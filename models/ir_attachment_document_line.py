# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by sebi.pulver on 29.03.2017.
#
# imports of python lib
import logging

# imports of openerp
from openerp import tools
from openerp import models, fields, api

# imports from odoo modules

# global variable definitions
_logger = logging.getLogger(__name__)


class IrAttachmentDocumentLine(models.Model):
    # Private attributes
    _name = 'ir.attachment.document.line'
    _description = 'Attachments of Document lines'
    _auto = False

    res_model = fields.Char(
            string="Model",
            readonly=True,
    )

    res_id = fields.Integer(
            string="Ressource Id",
    )

    attachment_id = fields.Many2one(
            comodel_name="ir.attachment",
            string="Attachment id",
    )

    attached_to_id = fields.Reference(
            string="Attached To",
            selection=[('product.product', 'Product Variant'),
                       ('product.template', 'Product')],
    )

    name = fields.Char(
            compute='_compute_name'
    )

    @api.multi
    def _compute_name(self):
        for record in self:
            name = ''
            if hasattr(record.attached_to_id, 'default_code'):
                name = '[{0}] '.format(record.attached_to_id.default_code)
            if hasattr(record.attached_to_id, 'name'):
                name += record.attached_to_id.name + ' - '
            name += record.attachment_id.name
            record.name = name

    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)

        cr.execute("""
CREATE or REPLACE VIEW ir_attachment_document_line as (
 SELECT DISTINCT sub.line_id * 10000 + att.id AS id,
    sub.res_model,
    sub.res_id,
    att.id AS attachment_id,
    (((att.res_model)::text || ','::text) || att.res_id) AS attached_to_id
   FROM (( SELECT 'sale.order'::text AS res_model,
            sol.order_id AS res_id,
            sol.product_id,
            sol.id AS line_id
           FROM sale_order_line sol
          WHERE (sol.product_id IS NOT NULL)
        UNION ALL
         SELECT 'purchase.order'::text AS res_model,
            pol.order_id AS res_id,
            pol.product_id,
            (pol.id * '-1'::integer) AS line_id
           FROM purchase_order_line pol
          WHERE (pol.product_id IS NOT NULL)) sub
     JOIN ir_attachment att ON (((att.res_id = sub.product_id) AND ((
     att.res_model)::text = 'product.product'::text))))
UNION ALL
 SELECT DISTINCT sub.line_id * 1000 + att.id  AS id,
    sub.res_model,
    sub.res_id,
    att.id AS attachment_id,
    (((att.res_model)::text || ','::text) || att.res_id) AS attached_to_id
   FROM (( SELECT 'sale.order'::text AS res_model,
            sol.order_id AS res_id,
            prod.product_tmpl_id,
            sol.id AS line_id
           FROM (sale_order_line sol
             JOIN product_product prod ON ((prod.id = sol.product_id)))
          WHERE (sol.product_id IS NOT NULL)
        UNION ALL
         SELECT 'purchase.order'::text AS res_model,
            pol.order_id AS res_id,
            prod.product_tmpl_id,
            (pol.id * '-1'::integer) AS line_id
           FROM (purchase_order_line pol
             JOIN product_product prod ON ((prod.id = pol.product_id)))
          WHERE (pol.product_id IS NOT NULL)) sub
     JOIN ir_attachment att ON (((att.res_id = sub.product_tmpl_id) AND ((
     att.res_model)::text = 'product.template'::text))))
)
""")
