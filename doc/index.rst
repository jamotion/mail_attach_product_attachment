Product Attachments on Mails
============================

Some words about why this module was created...

.. contents:: Table of Content
   :depth: 2

The following chapters will give you some inputs about configuration and usage
of the features:

Feature 1
---------

Usage
^^^^^

How to use it

Configuration Options
^^^^^^^^^^^^^^^^^^^^^

How to configure it

For a screenshot with parameters explained add a

.. figure:: screenshot-example.png
   :figwidth: 800px
   :align: center
   :height: 600px 
   :width: 800px
   :scale: 100 %
   :alt: alternate text

   Screenshot caption

   +-----------------------+-----------------------+
   | Parameter / Symbol    | Meaning               |
   +=======================+=======================+
   | Parameter name        | Parameter description |
   +-----------------------+-----------------------+
   | .. image:: button.png | Description of button |
   |    :height: 64px      |                       |
   |    :width:  64px      |                       |
   +-----------------------+-----------------------+


Todo's
------

- List of planned, but not yet implemented features
