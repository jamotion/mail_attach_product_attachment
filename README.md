Position Attachments on Mails
============================

This module provides a list of the attachments of order positions on the mail
wizard. When sending a mail from sale or purchase order, the attachments to
append can be selected.


Check the [documentation](https://bitbucket.org/jamotion/
mail_attach_product_attachment/src/8.0/doc/
index.rst?fileviewer=file-view-default) for further information...
