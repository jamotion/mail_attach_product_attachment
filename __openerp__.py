# -*- coding: utf-8 -*-
{
    'name': 'Product Attachments on Mails',
    'version': '8.0.1.0.0',
    'category': 'Social Network',
    'author':  'Jamotion GmbH',
    'website': 'https://jamotion.ch',
    'summary': 'Choose product attachments to sent by email',
    'images': [],
    'depends': [
        'sale',
        'purchase',
        'mail',
        'document',
        'mail_attach_existing_attachment',
    ],
    'data': [
        'wizards/mail_compose_message_view.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [],
    'test': [],
    'application': False,
    'active': False,
    'installable': True,
}
