# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by sebi.pulver on 28.03.2017.
#
# imports of python lib
import logging

# imports of openerp
from openerp import models, fields, api


class MailComposeMessage(models.TransientModel):
    # Private attributes
    _inherit = 'mail.compose.message'

    # Default methods

    # Fields declaration
    position_attachment_ids = fields.Many2many(
            comodel_name="ir.attachment.document.line",
            column2="id",
            relation='ir_att_doc_mail_compose_rel',
            string="Position Attachments",
    )

    # compute and search fields, in the same order that fields declaration
    @api.model
    def get_mail_values(self, wizard, res_ids):
        res = super(MailComposeMessage, self).get_mail_values(wizard, res_ids)
        if wizard.position_attachment_ids.ids and wizard.model and\
                len(res_ids) == 1:
            for res_id in res_ids:
                if not res[res_id].get('attachment_ids'):
                    res[res_id]['attachment_ids'] = []
                res[res_id]['attachment_ids'].extend(
                    wizard.position_attachment_ids.mapped('attachment_id.id')
                )
        return res
